/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID CHAMBER_AMBIENCE_LONG = 2073821741U;
        static const AkUniqueID CHAMBER_AMBIENCE_LOUD = 2191265067U;
        static const AkUniqueID CHIMES = 2590078866U;
        static const AkUniqueID CLOCK_WINDING = 3275534650U;
        static const AkUniqueID NARRATOR = 3272864290U;
        static const AkUniqueID TICK_TOCK = 4154522844U;
        static const AkUniqueID VAULT_AMBIENCE = 2511289762U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID SPATIALIZATION = 2395249601U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID BANK = 1744429087U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID CHAMBER = 1329445049U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID NARRATOR = 3272864290U;
        static const AkUniqueID REVERBS = 3545700988U;
        static const AkUniqueID VAULT = 1037843411U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID CHAMBERREVERB = 2378666113U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
