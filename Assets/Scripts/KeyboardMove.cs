using UnityEngine;

public class KeyboardMove : MonoBehaviour {
    public GameObject player;
    public GameObject camera;
    public float _speed;

    private Transform _camTransform;

    void Start () {
        _camTransform = camera.transform;
    }

    void Update () {
        CameraPositionKeyControl ();
    }

    private void CameraPositionKeyControl () {
        Vector3 campos = Vector3.zero;

        if (Input.GetKey (KeyCode.D)) { campos += _camTransform.right * Time.deltaTime * _speed; }
        if (Input.GetKey (KeyCode.A)) { campos -= _camTransform.right * Time.deltaTime * _speed; }
        if (Input.GetKey (KeyCode.E)) { campos += _camTransform.up * Time.deltaTime * _speed; }
        if (Input.GetKey (KeyCode.Q)) { campos -= _camTransform.up * Time.deltaTime * _speed; }
        if (Input.GetKey (KeyCode.W)) { campos += _camTransform.forward * Time.deltaTime * _speed; }
        if (Input.GetKey (KeyCode.S)) { campos -= _camTransform.forward * Time.deltaTime * _speed; }

        player.transform.position += campos;
    }

}