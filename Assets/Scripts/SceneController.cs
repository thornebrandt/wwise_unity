using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {
    public Transform chamberRoomSpawn;
    public Transform vaultRoomSpawn;
    public Collider doorCollider;
    public Collider screenCollider;
    public GameObject player;
    public UIController uiController;
    private int state = -1;
    private int lastState = -1;

    void Start () {
        state = 0;
    }

    void Update () {
        checkStateMachine ();
        checkMouseEvents ();
    }

    void checkStateMachine () {
        if (lastState != state) {
            Debug.Log ("changing to new state: " + state);
            lastState = state;
            switch (state) {
                case 0:
                    transportToChamber ();
                    break;
                case 1:
                    transportToVault ();
                    break;
                default:
                    Debug.Log ("changed to state: " + state);
                    break;
            }
        }
    }

    void checkMouseEvents () {
        if (Input.GetMouseButtonDown (0) && !uiController.showingMenu) {
            checkRaycast ();
        }
    }

    void checkRaycast () {
        Ray ray = Camera.main.ScreenPointToRay (new Vector3 (Screen.width / 2.0f, (Screen.height / 2.0f), 0.0f));
        RaycastHit hit;
        if (Physics.Raycast (ray, out hit)) {
            if (hit.collider == doorCollider) {
                switch (state) {
                    case 0:
                        //you are in chamber, transport to vault. 
                        state = 1;
                        break;
                    case 1:
                        //you are in vault, transport to chamber. 
                        state = 0;
                        break;
                    default:
                        Debug.Log ("not sure where you are, state: " + state);
                        break;
                }
            }
            if (hit.collider == screenCollider) {
                if (state == 0) {
                    triggerNarrator ();
                }
            }
        }
    }

    void triggerNarrator () {
        AkSoundEngine.PostEvent ("narrator", screenCollider.gameObject);
    }

    void transportToChamber () {
        player.transform.position = chamberRoomSpawn.position;
    }

    void transportToVault () {
        player.transform.position = vaultRoomSpawn.position;
    }
}