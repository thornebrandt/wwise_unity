using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    public GameObject settingsMenu;
    public MouseLook mouseLook;
    public KeyboardMove keyboardMove;
    public Slider spatializationSlider;
    public Toggle WASDToggle;
    public Animator animatedWall;
    [HideInInspector]
    public bool showingMenu;

    public void Start () {
        alignSettings ();

    }

    public void Update () {
        CheckKeyboard ();
    }

    private void CheckKeyboard () {
        if (Input.GetKeyDown (KeyCode.F1) || Input.GetKeyDown (KeyCode.Escape)) {
            if (settingsMenu != null) {
                ToggleSettingsMenu ();
            } else {
                Debug.Log ("no settings menu found");
            }
        }
    }

    private void alignSettings () {
        turnOffSettingsMenu ();
        SetRTCPSpatialization (spatializationSlider.value);
        turnOffAnimatedWall ();
        turnOffWASD ();
        setCursorLock ();
        showingMenu = settingsMenu.activeSelf;
    }

    private void turnOffAnimatedWall () {
        animatedWall.enabled = false;
    }

    private void turnOffSettingsMenu () {
        settingsMenu.SetActive (false);
    }

    private void turnOffWASD () {
        if (WASDToggle != null) {
            WASDToggle.isOn = false;
        } else {
            Debug.Log ("WASDToggle not set");
        }
        if (keyboardMove != null) {
            keyboardMove.enabled = false;
        } else {
            Debug.Log ("keyboardMove not set");
        }
    }

    private void ToggleSettingsMenu () {
        settingsMenu.SetActive (!settingsMenu.activeSelf);
        setCursorLock ();
        showingMenu = settingsMenu.activeSelf;
    }

    public void UpdateSpatializationSlider () {
        if (spatializationSlider != null) {
            SetRTCPSpatialization (spatializationSlider.value);
        } else {
            Debug.Log ("spatialization slider not set");
        }
    }

    private void SetRTCPSpatialization (float value) {
        float spatializationValue = Mathf.Lerp (0, 100, value);
        AkSoundEngine.SetRTPCValue ("spatialization", spatializationValue);
    }

    public void ToggleWASD () {
        if (WASDToggle != null) {
            Debug.Log ("setting toggle to " + WASDToggle.isOn);
            if (keyboardMove != null) {
                keyboardMove.enabled = WASDToggle.isOn;
            }
        } else {
            Debug.Log ("WASD toggle is not set!");
        }
    }

    private void setCursorLock () {
        if (mouseLook != null) {
            if (settingsMenu.activeSelf) {
                mouseLook.SetCursorLock (false);
            } else {
                mouseLook.SetCursorLock (true);
            }
        }
    }
}